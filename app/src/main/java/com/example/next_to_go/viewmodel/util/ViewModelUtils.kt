package com.example.next_to_go.viewmodel.util

import android.util.Log
import com.example.next_to_go.composes.FilterState
import com.example.next_to_go.data.viewDataModel.Category
import com.example.next_to_go.data.viewDataModel.Race

object ViewModelUtils {


    /**
     * get list of categories according the filter state
     *
     * @param filterState current state of checked items
     * @return List<[Category]>
     */
    fun getCheckedCategories(filterState: FilterState): List<Category> {
        val categories = mutableListOf<Category>()
        if (filterState.greyhoundChecked) {
            categories.add(Category.GreyHoundRacing)
        }
        if (filterState.horseChecked) {
            categories.add(Category.HorseRacing)
        }
        if (filterState.harnessChecked) {
            categories.add(Category.HarnessRacing)
        }
        return categories
    }

    /**
     * it sort the list according to advertisedStart in ascending
     * @param races current races
     * @return List<[Race]> returns ascending races list
     */
    fun sortRacesAscending(races: List<Race>) =
        races.sortedBy {
            it.advertisedStart
        }


    /**
     * It filter the races between according to categories
     * @param races current races
     * @param categories current active categories
     *
     * @return Lis<[Race]] it returns the list of races with selected categories only
     */
    fun filterByCategory(races: List<Race>, categories: List<Category>): List<Race> {
        val r = mutableListOf<Race>()
        if (categories.isEmpty() || categories.size == 3) {
            return races
        }
        categories.forEach { category ->
            r.addAll(races.filter {
                it.category == category
            })
        }
        return r
    }

    /**
     * @param advertisedTime time in seconds (Unix time)
     * @return returns true if different between current time and advertesed time is greater then then 1minute
     * else return false
     */
    private fun oneMinuteOrMoreLeft(advertisedTime: Int): Boolean {
        val currentTimeInSeconds = System.currentTimeMillis() / 1000
        if (advertisedTime - currentTimeInSeconds > 60) {
            return true
        }
        return false
    }

    /**
     * Check is @param newRaceId is in not in the @param races
     * @param newRaceId
     * @param races
     * @return returns true if newRaceId race is not in the list &
     * return false if the newRaceId race is in the list
     */
    private fun isNotSameRaceId(newRaceId: String?, races: List<Race>): Boolean {
        races.forEach { race ->
            if (race.raceId == newRaceId) {
                return false
            }
        }
        return true
    }



    /**
     * @param racesNeedFiltration current filtering list
     * @param need how many races we need
     * @param currentShowingList current showing list
     *
     * @return List<[Race]> returns the list which have 1minute left to commence
     */
    fun getRacesWhichHave1MinuteLeftToCommence(
        racesNeedFiltration: List<Race>,
        need: Int,
        currentShowingList: List<Race>
    ): List<Race> {
        val filteredRaces = mutableListOf<Race>()
        for (race in racesNeedFiltration) {
            if (filteredRaces.size == need) {
                break
            }
            val checkRaces = if (need == 1) currentShowingList else filteredRaces
            if (oneMinuteOrMoreLeft(race.advertisedStart!!) && isNotSameRaceId(
                    race.raceId,
                    checkRaces
                )
            ) {
                filteredRaces.add(race)
            }
        }
//        Log.d("races", "filteredRacesReturn:${filteredRaces.size} ")
        return filteredRaces
    }

}