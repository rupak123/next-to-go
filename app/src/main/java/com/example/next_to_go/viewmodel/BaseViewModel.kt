package com.example.next_to_go.viewmodel

import android.util.Log
import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.next_to_go.composes.FilterState
import com.example.next_to_go.data.repository.BaseRepository
import com.example.next_to_go.data.viewDataModel.Race
import com.example.next_to_go.viewmodel.util.ViewModelUtils.filterByCategory
import com.example.next_to_go.viewmodel.util.ViewModelUtils.getCheckedCategories
import com.example.next_to_go.viewmodel.util.ViewModelUtils.getRacesWhichHave1MinuteLeftToCommence
import com.example.next_to_go.viewmodel.util.ViewModelUtils.sortRacesAscending
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BaseViewModel @Inject constructor(
    private val baseRepository: BaseRepository
) : ViewModel() {

    private val _liveDataRaces = mutableStateListOf<Race>()
    val liveDataRaces= _liveDataRaces

    private val _filterState = MutableStateFlow(FilterState())
    val filterState: StateFlow<FilterState> = _filterState

    private var races = mutableListOf<Race>()

    init {
        viewModelScope.launch(Dispatchers.IO) {
            val next5Races = getNextRaces(numberOfRaces = 5)
            this@BaseViewModel.races = next5Races.toMutableList()
            _liveDataRaces.addAll(next5Races)
        }
    }


    /**
     * @param newFilterState filter the list according to current filter state
     */
    fun updateFilterState(newFilterState: FilterState) {
        viewModelScope.launch {
            _filterState.value = newFilterState

            _liveDataRaces.clear()
            val checkedCategories = getCheckedCategories(newFilterState)
            val filteredRaces = filterByCategory(races,checkedCategories)
            _liveDataRaces.addAll(filteredRaces)
        }
    }

    /**
     * @param race remove this race
     */
    fun removeRace(race: Race){
        _liveDataRaces.remove(race)
        races.remove(race)
        addNewRace()
    }

    /**
     * Add new Race
     */
    private fun addNewRace(){
        viewModelScope.launch(Dispatchers.IO) {
            val nextRace = getNextRaces(numberOfRaces = 1)[0]
            Log.d("races","addRace: ${nextRace}")
            this@BaseViewModel.races.add(nextRace)
            this@BaseViewModel.races = sortRacesAscending(this@BaseViewModel.races).toMutableList()
            updateFilterState(filterState.value)
        }
    }




    /**
     * @param numberOfRaces how many races we need
     * @return <List<Race>> list of races
     */
    private suspend fun getNextRaces(numberOfRaces: Int): List<Race>{
        val nextRaces = mutableListOf<Race>()
        while (nextRaces.size!=numberOfRaces){
            val allRaces = baseRepository.getRaces(10)
            val races = getRacesWhichHave1MinuteLeftToCommence(allRaces,numberOfRaces-nextRaces.size,this.races)
            nextRaces.addAll(races)
            Log.d("races","nextRaces size:${nextRaces.size}")
        }
        return sortRacesAscending(nextRaces)
    }

}