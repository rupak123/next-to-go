package com.example.next_to_go.utils

object  Utils {
    fun formatDuration(seconds: Long): String {
        val minutes = seconds / 60
        val remainingSeconds = seconds % 60

        val minutesStr = if (minutes < 10) "0$minutes" else "$minutes"
        val secondsStr = if (remainingSeconds < 10) "0$remainingSeconds" else "$remainingSeconds"

        return "${minutesStr}m ${secondsStr}s"
    }

}