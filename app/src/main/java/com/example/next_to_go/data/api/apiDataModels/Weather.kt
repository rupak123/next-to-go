package com.example.next_to_go.data.api.apiDataModels

import com.google.gson.annotations.SerializedName


data class Weather (

  @SerializedName("id"         ) var id        : String? = null,
  @SerializedName("name"       ) var name      : String? = null,
  @SerializedName("short_name" ) var shortName : String? = null,
  @SerializedName("icon_uri"   ) var iconUri   : String? = null

)