package com.example.next_to_go.data.api

import com.example.next_to_go.data.api.apiDataModels.NextToGoData
import retrofit2.http.GET
import retrofit2.http.Query

interface RaceApiService {

    @GET("racing/")
    suspend fun getRaces(
        @Query("method") method:String,
        @Query("count") count:Int
    ): NextToGoData
}