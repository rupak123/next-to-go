package com.example.next_to_go.data.repository

import android.util.Log
import com.example.next_to_go.data.api.RaceApiService
import com.example.next_to_go.data.viewDataModel.Race
import javax.inject.Inject


class BaseRepository @Inject constructor (
    private val raceApiService: RaceApiService
) {

    suspend fun getRaces(count: Int):List<Race> {
        val races = raceApiService.getRaces("nextraces",count)
        Log.d("races","${races.status} ${races.data?.nextToGoIds.toString()}")
        return races.toRaces()
    }


}