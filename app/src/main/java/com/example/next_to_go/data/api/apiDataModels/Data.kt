package com.example.next_to_go.data.api.apiDataModels

import com.google.gson.annotations.SerializedName


data class Data (

  @SerializedName("next_to_go_ids" ) var nextToGoIds   : ArrayList<String> = arrayListOf(),
  @SerializedName("race_summaries" ) var raceSummaries : Map<String, RaceSummary>?    = null

)