package com.example.next_to_go.data.viewDataModel

sealed class Response<T>(
    val data: T? = null,
    val errorMessage: String? = null
) {
    class Success<T>(data: T) : Response<T>(data)
    class Error<T>(errorMessage: String) : Response<T>(errorMessage = errorMessage)
}
