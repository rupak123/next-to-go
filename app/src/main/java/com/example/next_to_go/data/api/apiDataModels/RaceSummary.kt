package com.example.next_to_go.data.api.apiDataModels


data class RaceSummary(
    val race_id: String,
    val race_name: String,
    val race_number: Int,
    val meeting_id: String,
    val meeting_name: String,
    val category_id: String,
    val advertised_start: AdvertisedStart,
    val race_form: RaceForm,
    val venue_id: String,
    val venue_name: String,
    val venue_state: String,
    val venue_country: String
)