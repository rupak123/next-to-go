package com.example.next_to_go.data.api.apiDataModels

import com.example.next_to_go.data.viewDataModel.Category
import com.example.next_to_go.data.viewDataModel.Race
import com.google.gson.annotations.SerializedName


data class NextToGoData (

    @SerializedName("status"  ) var status  : Int?    = null,
    @SerializedName("data"    ) var data    : Data?   = Data(),
    @SerializedName("message" ) var message : String? = null

){
  fun toRaces():List<Race>{
    val races : MutableList<Race> = ArrayList()
    data?.nextToGoIds?.forEach {
    val race = Race()
      val raceSummary = data?.raceSummaries?.get(it)
      race.raceId = raceSummary?.race_id
      race.raceNumber = raceSummary?.race_number
      race.advertisedStart = raceSummary?.advertised_start?.seconds
      race.meetingName = raceSummary?.meeting_name
      race.category = Category.toCategory(raceSummary?.category_id)
      races.add(race)
    }
    return races.toList()
  }
}