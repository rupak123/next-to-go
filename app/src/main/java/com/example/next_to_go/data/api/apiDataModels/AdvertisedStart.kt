package com.example.next_to_go.data.api.apiDataModels

import com.google.gson.annotations.SerializedName


data class AdvertisedStart (

  @SerializedName("seconds" ) var seconds : Int? = null

)