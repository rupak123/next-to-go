package com.example.next_to_go.data.viewDataModel

enum class Category() {
    HorseRacing,HarnessRacing,GreyHoundRacing;


    companion object {
        const val Greyhound_Racing ="9daef0d7-bf3c-4f50-921d-8e818c60fe61"
        const val Harness_Racing ="161d9be2-e909-4326-8c2c-35ed71fb460b"
        const val Horse_Racing ="4a2788f8-e825-4d36-9894-efd4baf1cfae"
        fun toCategory(category: String?): Category?{
            if (category== Greyhound_Racing){
                return GreyHoundRacing
            }
            if (category== Harness_Racing){
                return HarnessRacing
            }
            if (category== Horse_Racing){
                return HorseRacing
            }
            return null
        }
    }
}
