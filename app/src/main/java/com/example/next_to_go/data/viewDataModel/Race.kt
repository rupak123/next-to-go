package com.example.next_to_go.data.viewDataModel

data class Race(
    var raceId: String? = null,
    var meetingName: String? = null,
    var raceNumber: Int? = null,
    var advertisedStart: Int? = null,
    var category: Category? = null
)