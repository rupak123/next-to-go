package com.example.next_to_go.composes

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.Checkbox
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.next_to_go.R

/**
 * Shows Row with three Checkbox of Horse, Harness, GreyHound categories
 *
 * @param filterState it will change according to this state
 *
 * @param onHorseCheckedChange do when Horse checkbox state changes
 * @param onHarnessCheckedChange do when Harness checkbox state changes
 * @param onGreyhoundCheckedChange do when Greyhound checkbox state changes
 */
@Composable
fun RaceFilter(
    filterState: State<FilterState>,
    onHorseCheckedChange: (Boolean) -> Unit,
    onHarnessCheckedChange: (Boolean) -> Unit,
    onGreyhoundCheckedChange: (Boolean) -> Unit,
) {
    Row(modifier = Modifier.padding(top = 10.dp), verticalAlignment = Alignment.CenterVertically) {
        Text(text = "Next_To_Races", modifier = Modifier.padding(start = 20.dp))
        var expanded by remember {
            mutableStateOf(false)
        }

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentSize(Alignment.TopEnd)
                .padding(end = 10.dp)
        ) {
            IconButton(onClick = { expanded = !expanded }) {
                Image(
                    painterResource(id = R.drawable.baseline_filter_list_24),
                    contentDescription = "More",
                    colorFilter = ColorFilter.tint(color = MaterialTheme.colorScheme.onBackground)
                )
            }

            DropdownMenu(
                expanded = expanded,
                onDismissRequest = { expanded = false }
            ) {

                Column {
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier.padding(end = 20.dp)
                    ) {
                        Checkbox(
                            checked = filterState.value.horseChecked,
                            onCheckedChange = onHorseCheckedChange,
                            Modifier.padding(start = 10.dp)
                        )
                        Text("Horse")
                    }
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier.padding(end = 20.dp)
                    ) {
                        Checkbox(
                            checked = filterState.value.harnessChecked,
                            onCheckedChange = onHarnessCheckedChange,
                            Modifier.padding(start = 10.dp)
                        )
                        Text("Harness")
                    }

                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier.padding(end = 20.dp)
                    ) {
                        Checkbox(
                            checked = filterState.value.greyhoundChecked,
                            onCheckedChange = onGreyhoundCheckedChange,
                            Modifier.padding(start = 10.dp)
                        )
                        Text("Greyhound")
                    }

                }
            }
        }
    }
}

data class FilterState(
    var horseChecked: Boolean = false,
    var harnessChecked: Boolean = false,
    var greyhoundChecked: Boolean = false
)


@Preview(showBackground = true)
@Composable
fun raceFilterPreview() {
    val filterState =
        mutableStateOf(FilterState(true, true, false))

    RaceFilter(filterState, onHorseCheckedChange = { checked ->

    }, onHarnessCheckedChange = { checked ->

    }, onGreyhoundCheckedChange = { checked ->
    })
}