package com.example.next_to_go.composes

import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.next_to_go.R
import com.example.next_to_go.data.viewDataModel.Category
import com.example.next_to_go.data.viewDataModel.Race


/**
 * @param races according to this race state we will show the list to the user
 * @param removeRace it callbacks the viewModel function to remove this race
 */
@Composable
fun RacesList(races: SnapshotStateList<Race>?, modifier: Modifier = Modifier,
              removeRace: (Race) -> Unit) {
    LazyColumn(
        Modifier
            .fillMaxHeight()
            .padding(start = 5.dp, end = 5.dp)) {
        items(races!!) {race->
            var icon: Painter? = null
            if (race.category== Category.HarnessRacing){
                icon = painterResource(id = R.drawable.harness_racing)
            }
            if (race.category== Category.HorseRacing){
                icon = painterResource(id = R.drawable.horse_racing)
            }
            if (race.category== Category.GreyHoundRacing){
                icon = painterResource(id = R.drawable.greyhound_racing)
            }
            Column(
                modifier = Modifier
                    .padding(4.dp)
                    .border(
                        2.dp,
                        MaterialTheme.colorScheme.outline,
                        shape = RoundedCornerShape(4.dp)
                    )
            ) {
                Row {
                    Text(
                        text = "${race.raceNumber}. ${race.meetingName}(${race.raceNumber})",
                        modifier = Modifier
                            .padding(start = 10.dp, top = 10.dp, end = 0.dp, bottom = 7.dp)
                    )
                    Spacer(
                        Modifier
                            .weight(1f)
                            .fillMaxHeight()
                    )
                    Image(icon!!,
                        contentDescription = "",
                        contentScale = ContentScale.Crop,
                        modifier = Modifier
                            .size(54.dp, 40.dp)
                            .padding(5.dp),
                        colorFilter = ColorFilter.tint(color = MaterialTheme.colorScheme.onBackground)
                    )

                }
                CountDownText(removeRace = removeRace,race)
            }

        }
    }
}

@Preview(showBackground = true)
@Composable
fun previewRacesList(){

    val currentTimeInSeconds:Int = System.currentTimeMillis().div(1000).toInt()
    val races = mutableStateListOf(
        Race("","Goku", 3, currentTimeInSeconds.plus(120),category = Category.HarnessRacing),
        Race("","Gofffku", 3, currentTimeInSeconds.plus(130), category = Category.HorseRacing),
        Race("","Gokfggu", 3, currentTimeInSeconds.plus(450), category = Category.HorseRacing),
        Race("","Gokgfgu", 3, currentTimeInSeconds.plus(790),category = Category.HarnessRacing),
        Race("","Gogfku", 3, currentTimeInSeconds.plus(80),category = Category.GreyHoundRacing),
    )
    RacesList(races, removeRace = {

    })
}
