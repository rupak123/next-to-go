package com.example.next_to_go.composes

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.next_to_go.R
import com.example.next_to_go.data.viewDataModel.Race
import com.example.next_to_go.utils.Utils
import kotlinx.coroutines.delay

/**
 * @param race
 * @param removeRace it callbacks the viewModel function to remove this race
 */
@Composable
fun CountDownText(removeRace: (Race) -> Unit, race: Race){

    val currentTimeInSeconds = System.currentTimeMillis()/1000
    var timeRemainingInSecond = race.advertisedStart?.minus(currentTimeInSeconds)!!

    var timeRemaining by remember {
        mutableStateOf(Utils.formatDuration(timeRemainingInSecond))
    }


    /**
     * if timeRemaining in the countdown is smaller then 1minute the we will remove the race by callback
     */
    LaunchedEffect(key1 = timeRemainingInSecond, block = {
        if (timeRemainingInSecond<60){
            removeRace(race)
        }
        while (timeRemainingInSecond>60){
            timeRemainingInSecond--
            timeRemaining = Utils.formatDuration(timeRemainingInSecond)
            if (timeRemainingInSecond<=60){
                removeRace(race)
                Log.d("races","removeRace")
            }
            delay(1000)
        }
    })

    Row(modifier = Modifier
        .fillMaxWidth()
        .padding(bottom = 5.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center) {
        Image(painter = painterResource(id = R.drawable.baseline_timer_24),
            colorFilter = ColorFilter.tint(color = MaterialTheme.colorScheme.onBackground),
            contentDescription = "timer")
        Text(
            text = timeRemaining, textAlign = TextAlign.Center
        )
    }

}


@Preview(showBackground = true)
@Composable
fun previewCountDown(){
    val currentTimeInSeconds:Int = System.currentTimeMillis().div(1000).toInt()
    val race = Race("","Goku", 3, currentTimeInSeconds.plus(112))

    CountDownText(removeRace = {},race)
}
