package com.example.next_to_go

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.next_to_go.composes.CountDownText
import com.example.next_to_go.composes.FilterState
import com.example.next_to_go.composes.RaceFilter
import com.example.next_to_go.composes.RacesList
import com.example.next_to_go.composes.previewRacesList
import com.example.next_to_go.composes.raceFilterPreview
import com.example.next_to_go.viewmodel.BaseViewModel
import com.example.next_to_go.data.viewDataModel.Race
import com.example.next_to_go.ui.theme.Next_to_goTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val baseViewModel by viewModels<BaseViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Next_to_goTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val filterState = baseViewModel.filterState.collectAsState()
                    Column {
                        RaceFilter(filterState = filterState, onHorseCheckedChange = { checked ->
                            baseViewModel.updateFilterState(filterState.value.copy(horseChecked = checked))
                        }, onHarnessCheckedChange = { checked ->
                            baseViewModel.updateFilterState(filterState.value.copy(harnessChecked = checked))
                        }, onGreyhoundCheckedChange = { checked ->
                            baseViewModel.updateFilterState(filterState.value.copy(greyhoundChecked = checked))
                        })
                        RacesList(baseViewModel.liveDataRaces, removeRace = { race ->
                            baseViewModel.removeRace(race)
                        })
                    }

                }
            }
        }
    }
}










@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    Next_to_goTheme {
        Column {
            raceFilterPreview()
            previewRacesList()
        }
    }
}

