package com.example.next_to_go.viewmodel.util

import com.example.next_to_go.composes.FilterState
import com.example.next_to_go.data.viewDataModel.Category
import com.example.next_to_go.data.viewDataModel.Race
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test


class ViewModelUtilsTest {

    private lateinit var races: List<Race>
    private lateinit var filterState:FilterState
    private var currentTimeInSeconds:Int = 0

    @Before
    fun setUp(){
        filterState = FilterState(horseChecked = true,harnessChecked = false,greyhoundChecked = true)
        currentTimeInSeconds = (System.currentTimeMillis()/1000).toInt()
        races = listOf(
            Race("id_1","Francis", 2, currentTimeInSeconds+112,Category.HorseRacing),
            Race("id_2","Tony", 4, currentTimeInSeconds+80,Category.HarnessRacing),
            Race("id_3","Brock", 1, currentTimeInSeconds+256,Category.HorseRacing)
        )
    }

    @Test
    fun getCheckedCategories() {
        val categories = ViewModelUtils.getCheckedCategories(filterState)

        Assert.assertEquals(listOf(Category.GreyHoundRacing,Category.HorseRacing),categories)
    }

    @Test
    fun sortRacesAscending() {
        val expectedRaces = listOf(
            Race("id_2","Tony", 4, currentTimeInSeconds+80,Category.HarnessRacing),
            Race("id_1","Francis", 2, currentTimeInSeconds+112,Category.HorseRacing),
            Race("id_3","Brock", 1, currentTimeInSeconds+256,Category.HorseRacing)
        )

        val sortedRaces = ViewModelUtils.sortRacesAscending(races)
        Assert.assertEquals(expectedRaces,sortedRaces)
    }

    @Test
    fun filterByCategory() {
        val filteredRaces = ViewModelUtils.filterByCategory(races, ViewModelUtils.getCheckedCategories(filterState))
        val expectedRaces = listOf(
            Race("id_1","Francis", 2, currentTimeInSeconds+112,Category.HorseRacing),
            Race("id_3","Brock", 1, currentTimeInSeconds+256,Category.HorseRacing)
        )

        Assert.assertEquals(expectedRaces,filteredRaces)
    }

    @Test
    fun getRacesWhichHave1MinuteLeftToCommence() {
        val unFilteredRaces = listOf(
            Race("id_2","Tony", 4, currentTimeInSeconds+80,Category.HarnessRacing),
            Race("id_1","Francis", 2, currentTimeInSeconds+34,Category.HorseRacing),
            Race("id_3","Brock", 1, currentTimeInSeconds+256,Category.HorseRacing)
        )
        val expectedRaces = listOf(
            Race("id_2","Tony", 4, currentTimeInSeconds+80,Category.HarnessRacing),
            Race("id_3","Brock", 1, currentTimeInSeconds+256,Category.HorseRacing)
        )
        val filteredRaces = ViewModelUtils.getRacesWhichHave1MinuteLeftToCommence(unFilteredRaces,need= 2,unFilteredRaces)

        Assert.assertEquals(expectedRaces,filteredRaces)
    }


    @After
    fun tearDown(){

    }
}