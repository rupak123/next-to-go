package com.example.next_to_go.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.next_to_go.data.repository.BaseRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.Assertions.*
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@OptIn(ExperimentalCoroutinesApi::class)
class BaseViewModelTest {

    private val testDispatcher = StandardTestDispatcher()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var repository: BaseRepository

    private lateinit var mockWebServer: MockWebServer

    @Before
    fun setUp(){
        MockitoAnnotations.openMocks(this)
        Dispatchers.setMain(testDispatcher)
    }


    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun testInit() = runTest {
//        val data = ApiUtil.readFileResource("/locale_response.json")
        Mockito.`when`(repository.getRaces(0)).thenReturn(listOf())

        val baseViewModel = BaseViewModel(repository)

        testDispatcher.scheduler.advanceUntilIdle()
        val races = baseViewModel.liveDataRaces

        Assert.assertEquals(0,races.size)
    }



    @Test
    fun getLiveDataRaces() {

    }

    @Test
    fun getFilterState() {
    }

    @Test
    fun updateFilterState() {
    }

    @Test
    fun removeRace() {
    }


    @After
    fun tearDown() {
    }
}