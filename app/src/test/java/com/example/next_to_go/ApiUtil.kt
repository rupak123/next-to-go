package com.example.next_to_go

import java.io.InputStreamReader

object ApiUtil {
    fun readFileResource(filename: String):String{
        val inputStream = ApiUtil::class.java.getResourceAsStream(filename)
        val stringBuilder = StringBuilder()
        val inputStreamReader = InputStreamReader(inputStream,"UTF-8")
        inputStreamReader.readLines().forEach {
            stringBuilder.append(it)
        }
        return stringBuilder.toString()
    }
}