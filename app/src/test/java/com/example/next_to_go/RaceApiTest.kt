package com.example.next_to_go

import com.example.next_to_go.data.api.RaceApiService
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RaceApiTest {

    private lateinit var mockWebServer: MockWebServer
    private lateinit var raceApiService: RaceApiService

    @Before
    fun setUp(){
        mockWebServer = MockWebServer()
        raceApiService = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(RaceApiService::class.java)


    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun testApiForNullResponse() = runTest {
        val mockResponse = MockResponse()
        mockResponse.setBody("{}")
        mockWebServer.enqueue(mockResponse)


        val response = raceApiService.getRaces("nextraces",10)
        mockWebServer.takeRequest()

        Assert.assertEquals(true,response.status==null)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun testApiForSuccessResponse() = runTest {
        val mockResponse = MockResponse()
        val data = ApiUtil.readFileResource("/locale_response.json")
        mockResponse.setBody(data)
        mockResponse.setResponseCode(200)
        mockWebServer.enqueue(mockResponse)


        val response = raceApiService.getRaces("nextraces",10)
        mockWebServer.takeRequest()

        Assert.assertEquals(true,response.data?.raceSummaries?.size?.equals(10))
    }


    @After
    fun tearDown(){
        mockWebServer.shutdown()
    }
}