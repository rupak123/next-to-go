package com.example.next_to_go.utils

import org.junit.Assert
import org.junit.Test


class UtilsTest {

    @Test
    fun formatDuration() {
        val seconds:Long = 78
        val timeString = Utils.formatDuration(seconds)

        Assert.assertEquals("01m 18s",timeString)
    }
}